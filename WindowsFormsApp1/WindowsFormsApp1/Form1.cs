﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        int firstnumber;
        int secondnumber;
        int output;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            firstnumber = int.Parse(txt_FirstNumber.Text);

            secondnumber = int.Parse(txt_SecondNumber.Text);

            output = firstnumber + secondnumber;

            MessageBox.Show("The Result is: "+output.ToString());
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e) {

        }

        private void button1_Click_1(object sender, EventArgs e) {
            firstnumber = int.Parse(txt_FirstNumber.Text);

            secondnumber = int.Parse(txt_SecondNumber.Text);

            output = firstnumber - secondnumber;

            MessageBox.Show("The Result is: " + output.ToString());
        }
    }
}
